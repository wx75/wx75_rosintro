#!/bin/bash

# Adapted from provided tutorial code for this HW

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from math import pi, tau, dist, fabs, cos
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


def all_close(goal, actual, tolerance):
    # For testing if the values in two lists are within a tolerance of each other.
    
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True

class MyPyUI(object):
    def __init__(self):
        # initialize the nodes
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )
        planning_frame = move_group.get_planning_frame()
        eef_link = move_group.get_end_effector_link()
        group_names = robot.get_group_names()

        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self):
        # move the robot according to a joint space goal
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = 0
        joint_goal[2] = 0
        joint_goal[3] = 0
        joint_goal[4] = 0

        move_group.go(joint_goal, wait=True)
        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def go_to_pose_goal(self):
        # move the robot according to a operational space goal
        # Go to the starting point
        move_group = self.move_group

        # upper-left corner of "V"
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.x = 0.7
        pose_goal.orientation.y = 0.7
        pose_goal.orientation.z = 0
        pose_goal.orientation.w = 0
        pose_goal.position.x = 0.1
        pose_goal.position.y = 0.2
        pose_goal.position.z = 0.5
        move_group.set_pose_target(pose_goal)
        
        success = move_group.go(wait=True)
        move_group.stop()
        # move_group.clear_pose_targets()


        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)

    def plan_cartesian_path(self, scale=1):
        # generate a trajectory according to a series of waypoints in operational space
        move_group = self.move_group

        waypoints = []
        width=0.15
        height=0.35

        # bottom corner of "V"
        wpose = move_group.get_current_pose().pose
        wpose.position.x += scale * width
        wpose.position.z -= scale * height
        waypoints.append(copy.deepcopy(wpose))

        # upper-right corner of "V" & upper-left corner of "X"
        wpose.position.x += scale * width
        wpose.position.z += scale * height
        waypoints.append(copy.deepcopy(wpose))

        # bottom-right corner of "X"
        wpose.position.x += scale * 2*width
        wpose.position.z -= scale * height
        waypoints.append(copy.deepcopy(wpose))

        # upper-right corner of "X"
        wpose.position.z += scale * height
        waypoints.append(copy.deepcopy(wpose))

        # bottom-left corner of "X"
        wpose.position.x -= scale * 2*width
        wpose.position.z -= scale * height
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0# eef_step, jump_threshold
        )

        self.display_trajectory(plan)
        print(">>>waypoints:"+str(waypoints))
        return plan, fraction
        
    def display_trajectory(self, plan):
        # dispaly trajectory for visualization
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        display_trajectory_publisher.publish(display_trajectory)

    def execute_plan(self, plan):
        # move the robot according to the generated trajectory
        move_group = self.move_group
        move_group.execute(plan, wait=True)

def main():
    # execution pipeline and a simple UI
    try:
        print("Press Ctrl-D to exit at any time")

        input(
            "=== Press `Enter` to start"
        )
        moveRobot = MyPyUI()

        input(
            "=== Press `Enter` to re-center the robot with state goal"
        )
        moveRobot.go_to_joint_state()

        input("=== Press `Enter` to let the robot ready at start position with a pose goal")
        moveRobot.go_to_pose_goal()

        input("=== Press `Enter` to plan and display a Cartesian path")
        cartesian_plan, fraction = moveRobot.plan_cartesian_path()

        input("=== Press `Enter` to execute the path")
        moveRobot.execute_plan(cartesian_plan)

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
