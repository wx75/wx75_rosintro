#!/bin/bash

# Victor Xia, 10/17/2022
# Adapted from provided tutorial code for this project

import sys
import copy
import rospy
import moveit_commander

class vx(object):
    # a collection of methods to setup the goals for the robot to draw "VX"

    def startingPoint(manipulator):
        # Generate the starting point of drawing "VX" in operational space

        # upper-left corner of "V"
        pose=manipulator.get_current_pose().pose
        pose.orientation.x=0.7
        pose.orientation.y=0.7
        pose.orientation.z=0
        pose.orientation.w=0
        pose.position.x=0.2
        pose.position.y=0.1
        pose.position.z=0.5

        return pose

    def path(manipulator):
        # This is a function to generate the waypoints of my initial "VX"

        # Use a existing pose as a template for duplication
        pose=manipulator.get_current_pose().pose

        waypoints=[]
        width=0.12
        height=0.3
        scale=1

        # bottom corner of "V"
        pose.position.x+=scale*width
        pose.position.z-=scale*height
        waypoints.append(copy.deepcopy(pose))

        # upper-right corner of "V" & upper-left corner of "X"
        pose.position.x+=scale*width
        pose.position.z+=scale*height
        waypoints.append(copy.deepcopy(pose))

        # bottom-right corner of "X"
        pose.position.x+=scale*2*width
        pose.position.z-=scale*height
        waypoints.append(copy.deepcopy(pose))

        # upper-right corner of "X"
        pose.position.z+=scale*height
        waypoints.append(copy.deepcopy(pose))

        # bottom-left corner of "X"
        pose.position.x-=scale*2*width
        pose.position.z-=scale*height
        waypoints.append(copy.deepcopy(pose))

        return waypoints

class my_UR5eUI(object):
    # My user interface to give commands conveniently to UR5e
    def __init__(self):
        # initialize the node
        rospy.init_node("myUR5eExercise", anonymous=True)
        moveit_commander.roscpp_initialize(sys.argv)
        robot=moveit_commander.RobotCommander()
        scene=moveit_commander.PlanningSceneInterface()
        manipulator=moveit_commander.MoveGroupCommander("manipulator")

        self.robot=robot
        self.scene=scene
        self.manipulator=manipulator

    def neutralPoint(self):
        # Generate a neutral point of the UR5e robot arm
        # It's a good practice to move the robot to this neutral point before assigning any new task
        manipulator=self.manipulator

        allJointZero=manipulator.get_current_joint_values()
        allJointZero[0]=0
        allJointZero[1]=0
        allJointZero[2]=0
        allJointZero[3]=0
        allJointZero[4]=0
        allJointZero[5]=0

        return allJointZero

    def moveInJointSpace(self,JSGoal):
        # move the robot according to a joint space goal
        manipulator=self.manipulator

        manipulator.go(JSGoal, wait=True)
        manipulator.stop()

        currentState=manipulator.get_current_joint_values()
        print("The current joint space values are"+str(currentState))
        return self.checkIfDone(JSGoal,currentState,0.01)

    def moveInOperationalSpace(self,OSGoal):
        # move the robot according to a operational space goal
        manipulator=self.manipulator

        manipulator.set_pose_target(OSGoal)
        go=manipulator.go(wait=True)
        manipulator.stop()

        currentState=manipulator.get_current_joint_values()
        print("The current joint space values are"+str(currentState))

        currentState=self.manipulator.get_current_pose().pose
        return self.checkIfDone(OSGoal,currentState,0.01)

    def planTraj(self,waypoints):
        # generate a trajectory according to a series of waypoints in operational space
        manipulator=self.manipulator

        (traj, fraction)=manipulator.compute_cartesian_path(
            waypoints, 0.01, 0.0)

        print(">>>waypoints:"+str(waypoints))
        return traj, fraction

    def moveAlongTraj(self,traj):
        # move the robot according to the generated trajectory
        manipulator=self.manipulator
        manipulator.execute(traj, wait=True)
        
        currentState=manipulator.get_current_joint_values()
        print("The current joint space values are"+str(currentState))

    def checkIfDone(self,goal,actual,tolerance):
        ''' 
        -- For testing if the values in two lists are within a tolerance of each other.

        -- The commented part below is copied from the tutorial code

        -- For this project I would just assume that the task was completed within tolerence
            as I prefer to avoid using copied code
        '''
        
        # import geometry_msgs.msg
        # from math import pi, tau, dist, fabs, cos

        # if type(goal) is list:
        #     for index in range(len(goal)):
        #         if abs(actual[index] - goal[index]) > tolerance:
        #             return False

        # elif type(goal) is geometry_msgs.msg.PoseStamped:
        #     return checkIfDone(goal.pose, actual.pose, tolerance)

        # elif type(goal) is geometry_msgs.msg.Pose:
        #     x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        #     x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        #     # Euclidean distance
        #     d = dist((x1, y1, z1), (x0, y0, z0))
        #     # phi=angle between orientations
        #     cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        #     return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

        return True

def main():
    # execution pipeline and a simple UI
    print("=== Press Ctrl-D to exit at any time ===")

    input("=== Press `Enter` to initialize ===")
    commandUR5e=my_UR5eUI()
    manipulator=commandUR5e.manipulator

    input("=== Press `Enter` to re-center the robot to its nuetral point with a joint space goal ===")
    goal=commandUR5e.neutralPoint()
    commandUR5e.moveInJointSpace(goal)

    input("=== Press `Enter` to let the robot ready at start position with a pose goal ===")
    goal=vx.startingPoint(manipulator)
    commandUR5e.moveInOperationalSpace(goal)

    input("=== Press `Enter` to plan and display a Cartesian path ===")
    waypoints=vx.path(manipulator)
    traj, fraction=commandUR5e.planTraj(waypoints)

    input("=== Press `Enter` to execute the path ===")
    commandUR5e.moveAlongTraj(traj)

if __name__ == "__main__":
    main()
