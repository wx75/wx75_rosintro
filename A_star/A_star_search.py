import math
import matplotlib as mlib
import matplotlib.pyplot as plt
import copy

def Euclid(p1,p2):
	'''Calculates 2-norm or Euclidean distance between two points'''
	return math.sqrt((p1.x-p2.x)**2 + (p1.y-p2.y)**2)

def Manhat(p1,p2):
	'''Calculates 1-norm or Manhattan distance between two points'''
	return abs(p1.x-p2.x)+abs(p1.y-p2.y)

class Node:
	'''
	Saves info of a map node

	Properties:
		x 		<--- x coordinate
		y 		<--- y coordinate
		name 		<--- chr() alphabetical name of the node (i.e. 'A')
		neighbors 	<--- a list of neighboring Nodes
	
	methods:
		__init__(x,y,name)
		add_neighbors(pt) <--- add Node pt into "neighbors" list
	''' 
	
	def __init__(self, x,y,name):
		self.x=x
		self.y=y
		self.name=name

		self.neigbors=[]

	def add_neigbors(self,pt):
		'''add Node pt into "neighbors" list'''
		if not pt in self.neigbors:
			self.neigbors.append(pt)
		else:
			raise Exception("Point already in neigbors list")

class Path: 
	'''
	Contains info ofa path

	Properties:
		waypoints 	<--- Nodes traveled from/through/to, default=[]
		dist=0 		<--- distance traveled along the path, default=0

	Methods:
		__init__(waypoints=[],dis=0)
	'''
	def __init__(self, waypoints=[], dist=0):
		self.waypoints=waypoints
		self.dist=dist

class A_star:
	'''
	Contains info and methods related with A* algorithm

	Properties:
		map 	<--- a dictonary with {chr(name):Node(node)}

		start 	<---Node starting point
		goal 	<---Node end point

		todo 	<-- list(priority queue) of Paths
		visited <-- list of visited Node

		max_it 	<--- maximum search iteration, default=1000
	
	methods:
		__init__(map,start,goal,max_it=1000)

		plot(path=[]) 		<--- plot out the map and the Path

		print_out_process() <--- print out the visited and todo lists 
									for debugging and HW purposes

		heuristic_dist(pt) 	<--- calculate heuristic distance between the Node pt 
									and the goal Node

		visit_and_see(current_path) <---Visit the last Node of the Path current_path, 
										and see/extend to new Paths
										Pushes the new Paths to todo list

		search() <--- Continue looking at todo list
						until find optimal Path
						Returns the optimal Path
	''' 

	def __init__(self, map,start,goal,max_it=1000):
		self.map=map
		self.start=map[start]
		self.goal=map[goal]

		self.todo=[]
		self.visited=[]

		self.todo.append(Path(waypoints=[self.start]))

		self.max_it=max_it

	def plot(self,path=None):
		'''plot out the map and the Path'''

		# prepare a figure to plot on
		plt.figure()
		fig, ax=plt.subplots()
		ax.axis('equal')
		ax.margins(x=0.1,y=0.1)

		# plot nodes and their names
		for pt in self.map.values():
			ax.plot(pt.x,pt.y,'ko')
			for neigbor in pt.neigbors:
				ax.plot([pt.x, neigbor.x],[pt.y, neigbor.y], 'k-')
			ax.text(pt.x+0.07,pt.y+0.1,pt.name,color='black',fontsize=16)

		# plot start and goal points
		ax.plot(self.start.x, self.start.y, 'bo', markersize=10)
		ax.plot(self.goal.x, self.goal.y, 'ro', markersize=10)

		# it a path is provided, draw the path as well
		if path:
			lines=[[path.waypoints[i],path.waypoints[i+1]] for i in range(len(path.waypoints)-1)]
			for pt1, pt2 in lines:
				ax.plot([pt1.x, pt2.x],[pt1.y, pt2.y], 'g-')

	def print_out_process(self):
		'''
		Print out the visited and todo lists 
		for debugging and HW purposes
		'''
		print_visited=[i.name for i in self.visited]
		print("Visited: {}".format(print_visited))

		print("Todo: ")
		for path in self.todo:
			print_path=[i.name for i in path.waypoints]
			print(print_path,"={}+{}".format(int(path.dist),self.heuristic_dist(path.waypoints[-1])))
		print("")

	def heuristic_dist(self, pt):
		'''calculate heuristic distance between Node pt and the goal Node'''
		return Manhat(pt, self.goal)

	def visit_and_see(self, current_path):
		'''
		Visit the last Node of the Path current_path, 
		and see/extend to new Paths
		Pushes the new Paths to todo list
		'''
		current_node=current_path.waypoints[-1]
		self.visited.append(current_node)
		# take last node of the current_path

		# extend to new paths, add to todo list
		to_go=current_node.neigbors
		for pt in to_go:
			if pt not in self.visited:
				new_path=copy.deepcopy(current_path)
				new_path.waypoints.append(pt)
				new_path.dist+=Euclid(current_node,pt)

				self.todo.append(new_path)

		self.todo.sort(key=lambda i: i.dist+self.heuristic_dist(i.waypoints[-1]))
		# sort todo list by expected cost = actual cost + heuristic cost to go

		self.print_out_process()
		# print out the log

	def search(self):
		'''
		Continue looking at todo list
		until find optimal Path
		Returns the optimal Path
		'''
		iter=0
		while(self.todo and iter<=self.max_it):
			iter+=1

			current_path=self.todo[0]
			self.todo.pop(0)
			# take top path of the todo list

			current_node=current_path.waypoints[-1]
			# take last node of the current_path

			#check if already reached the goal
			if current_node is self.goal:
				print_path=[i.name for i in current_path.waypoints]
				print_dist=current_path.dist
				print("Optimal path found! {}, "\
					"with total traveld distance of {}".format(print_path, print_dist))
				self.plot(current_path)
				return current_path

			#expand search
			if current_node not in self.visited:
				self.visit_and_see(current_path)


		#maximum iteration exception
		if iter==self.max_it:
			raise Exception("Maximum iteration of {} reached, "\
				"but did not find any path!".format(self.max_it))
		#empty to list exception
		if not todo:
			raise Exception("Todo list exhausted, "\
				"but did not find any path!")

def MyMapDescription():
	'''
	High level description of the map,
	used to generate such a map.
	Returns a dictionaries with {chr(point names): Nodes(nodes)}
	'''

	xlim=5
	ylim=5
	last_name='W'

	gap={'R':1,'M':1}
	# when to make gaps in the grid to avoid obstacles, 
	# and how much to gap

	# make a list of all points' names
	namelist=[]
	for name in range(ord('A'),ord(last_name)+1):
		name=chr(name)
		namelist.append(name)
		if name not in gap:
			gap[name]=0

	# make nodes on the grid
	x=-1
	y=-1
	map={}
	for name in namelist:
		x+=1-xlim*(x>=xlim-1)+gap[name]
		y+=1*(x==0)
		map[name]=Node(x=x,y=y,name=name)

	# add edges
	for pt1 in map.values():
		for pt2 in map.values():
			if pt1 is not pt2:
				if not pt2 in pt1.neigbors:
					if Euclid(pt1,pt2)<1.1:
						pt1.neigbors.append(pt2)
						pt2.neigbors.append(pt1)
	return map

def main():
	start='G'
	goal='U'

	map=MyMapDescription()

	algorithm=A_star(map,start,goal)
	algorithm.search()


if __name__ == '__main__':
	main()
